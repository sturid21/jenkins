FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD ./target/maven-demo-1.2.7-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]